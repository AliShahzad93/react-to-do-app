import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <div className="container">
      <div id="container" className="col-md-8 col-md-offset-2">  </div>
    <TodoApp />
    </div> 
      </div>
      
    );
  }
}
const TodoForm = ({addTodo}) => {
  // Input tracker
  let input;

  return (
    <div>
      <input ref={node => {
        input = node;
      }}  />
      <button type="submit" onClick={() => {
        addTodo(input.value);
        input.value = '';
      }}>
        +
      </button>
    </div>
  );
};
const Todo = ({todo, remove,done}) => {
  // Each Todo
  return (<li> <input type="checkbox" onClick={() => {done(todo.id)}}/>{todo.text}           
  <button  onClick={() => {remove(todo.id)}}>x</button></li>);
}

const TodoList = ({todos, remove, done}) => {
  // Map through the todos
  const todoNode = todos.map((todo) => {
    return (<Todo todo={todo} key={todo.id} remove={remove} done={done}/>)
  });
  return (<ul>{todoNode}</ul>);
}
const Title = () => {
  return (
    <div>
       <div>
          <h1>To-do App</h1>
       </div>
    </div>
  );
}
var temp=0;
const Stats = ({fins}) => {
 // return( <h1>Done Tasks</h1>);
  
  var finNodes=fins.map((todo)=>{
  return (<li key={temp++}>{todo.text}</li>)
});

  return ( 
  <ul>{finNodes}</ul>);
} 

window.id = 0;
class TodoApp extends React.Component{
  constructor(props){
    // Pass props to parent class
    super(props);
    // Set initial state
    this.state = {
      data: [],
      done: []
    }
  }
  // Add todo handler
  addTodo(val){
    // Assemble data
    const todo = {text: val, id: window.id++}
    // Update data
    this.state.data.push(todo);
    // Update state
    this.setState({data: this.state.data});
  }
  // Handle remove
  handleRemove(id){
    // Filter all todos except the one to be removed
    const remainder = this.state.data.filter((todo) => {
      if(todo.id !== id) return todo;
    });
    
    // Update state with filter
    this.setState({data: remainder});
  }
  handleDone(id){
    
  const finished=this.state.data.filter((todo) => {
    
      if(todo.id === id) 
      return todo;
    });
    if (this.state.done.includes(finished[0])===false)
    {console.log("not there")
  this.state.done=this.state.done.concat(finished);
this.setState({done:this.state.done} );}
else{
  const index = this.state.done.indexOf(finished[0]);
  this.state.done.splice(index, 1);
  this.setState({done:this.state.done});
}
 
    //console.log(finished.id);
   // this.state.done.push(finished);
//    this.setState({done:this.state.done});

}

  render(){
    // Render JSX
    return (
      <div>
        <Title />
        <TodoForm addTodo={this.addTodo.bind(this)}/>
        <TodoList 
          todos={this.state.data} 
          remove={this.handleRemove.bind(this)}
          done={this.handleDone.bind(this)}
        />
        <h3>Tasks Completed</h3>
        <Stats
        fins={this.state.done}
        />
      </div>
    );
  }
}

export default App;
